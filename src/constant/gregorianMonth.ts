const GregorianMonth = {
    1: { title: "January", value: 1 },
    2: { title: "February", value: 2 },
    3: { title: "March", value: 3 },
    4: { title: "April", value: 4 },
    5: { title: "May", value: 5 },
    6: { title: "June", value: 6 },
    7: { title: "July", value: 7 },
    8: { title: "August", value: 8 },
    9: { title: "September", value: 9 },
    10: { title: "October", value: 10 },
    11: { title: "November", value: 11 },
    12: { title: "December", value: 12 },
};
export default GregorianMonth;
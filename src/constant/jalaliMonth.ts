

const JalaliMonth = {
    farvardin: "فروردین",
    ordibehesht: "اردیبهشت",
    xordad: "خرداد",
    tir: "تیر",
    mordad: "مرداد",
    shahrivar: "شهریور",
    mehr: "مهر",
    aban: "آبان",
    azar: "آذر",
    dey: "دی",
    bahman: "بهمن",
    esfand: "اسفند",
};
export default JalaliMonth;
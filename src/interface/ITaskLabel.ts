
interface ITaskLabel {
	color: string;
	title: string;
}

export default ITaskLabel;
export function getTimestamp(year: number, month: number, day: number) {
    const date = year + "/" + month + "/" + day;
    let t: string = new Date(date).getTime().toString();
    t = t.substring(0, t.length - 3);
    return t;
}
export function getMonth(): number {
    const today = new Date();
    return today.getMonth() + 1;
}
export function getYear(): number {
    const today = new Date();
    return today.getFullYear();
}
export function isLeapYear(year: number) {
    const ary = [1, 5, 9, 13, 17, 22, 26, 30];
    let b = year % 33;
    if (ary.includes(b)) {
        return true;
    } else {
        return false;
    }
}
export function jalaliToGregorian(jy: number, jm: number, jd: number) {
    let gy = jy <= 979 ? 621 : 1600;
    jy -= jy <= 979 ? 0 : 979;
    var days =
        365 * jy +
        (jy / 33) * 8 +
        ((jy % 33) + 3) / 4 +
        78 +
        jd +
        (jm < 7 ? (jm - 1) * 31 : (jm - 7) * 30 + 186);
    gy += 400 * (days / 146097);
    days %= 146097;
    if (days > 36524) {
        gy += 100 * (--days / 36524);
        days %= 36524;
        if (days >= 365) days++;
    }
    gy += 4 * (days / 1461);
    days %= 1461;
    gy += (days - 1) / 365;
    if (days > 365) days = (days - 1) % 365;
    var gd = days + 1;
    var sal_a = [
        0,
        31,
        (gy % 4 == 0 && gy % 100 != 0) || gy % 400 == 0 ? 29 : 28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31,
    ];
    var gm;
    for (gm = 0; gm < 13; gm++) {
        var v = sal_a[gm];
        if (gd <= v) break;
        gd -= v;
    }
    return [gy, gm, gd];
}
export function formatDate(date: Date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
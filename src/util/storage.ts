export function set(key: string, value: any) {
    if (key?.length > 0 && value?.length > 0) {
        window.localStorage.setItem(key, value);
    }
    return "";
};
export function get(key: string) {
    if (key?.length > 0) {
        const value = window.localStorage.getItem(key);
        if (value && value.length > 0) {
            return value;
        }
    }
    return "";
};
export function remove(key: string): void {
    if (key?.length > 0) {
        window.localStorage.removeItem(key);
    }
};
export function setJson(key: string, value: any) {
    if (key?.length > 0 && value?.length > 0) {
        window.localStorage.setItem(key, JSON.stringify(value));
    }
};
export function getJson(key: string) {
    if (key?.length > 0) {
        return JSON.parse(window?.localStorage?.getItem(key)!);
    }
};
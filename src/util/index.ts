import * as date from "./date";
import * as storage from "./storage";

const utility = {
    date,
    storage,
};

export default utility;

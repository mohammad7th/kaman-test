import { createApp } from 'vue';
import './assets/styles/main.scss';
import App from './App.vue';
import { createRouter, createWebHistory } from 'vue-router';
import Route from "./routes";

const route = createRouter({
    history: createWebHistory(),
    routes: Route
});
const app = createApp(App);
app.use(route);
app.mount('#app');

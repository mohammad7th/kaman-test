import { ITaskLabel } from "@/interface";

export class TaskLabel implements ITaskLabel {
    constructor() {

    }
    public title: string;
    public color: string;
}

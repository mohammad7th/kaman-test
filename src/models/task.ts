import { TaskLabel } from "@/models";
export class Task {
    constructor() {

    }
    public title: string;
    public date: string;
    public time?: string;
    public label?: TaskLabel;

}

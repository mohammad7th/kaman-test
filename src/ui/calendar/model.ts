export class Day {
    day: string | number;
    month: number;
    month_name: string;
    year: number;
    is_today: boolean;
}
export class Week {
    days: Day[];
}
export enum IconEnums {
	BACK = "icon-arrow_back",
	CLOSE = "icon-clear",
	PLUS = "icon-add",
	SEARCH = "icon-search",
	MENU = "icon-menu",
}

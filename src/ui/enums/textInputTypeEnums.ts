export enum TextInputTypeEnums {
	DEFAULT = "text",
	TEXT = "text",
	NUMBER = "number",
	TEL = "tel",
	PASSWORD = "password",
	EMAIL = "email",
	URL = "url",
}
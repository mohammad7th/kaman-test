export enum ButtonRoundedEnums {
	NONE = "rounded-none",
	NORMAL = "rounded",
	FULL = "rounded-full",
}

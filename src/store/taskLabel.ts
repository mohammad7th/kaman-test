import { createStore } from 'vuex';
import { ITaskLabel } from '@/interface';
import utility from '@/util';
interface ILabelStore {
    labels: ITaskLabel[]
}
const taskLabelStore = createStore<ILabelStore>({
    state() {
        return {
            labels: utility.storage.getJson('__labels') || [],
        }
    },
    getters: {
        list(state): ITaskLabel[] {
            return state.labels;
        }
    },
    mutations: {
        saveLabel(state, label: ITaskLabel) {
            if (state.labels.length > 0 && state.labels.find((t) => t.title === label.title && t.color === label.color)) {
                return;
            }
            state.labels = [...state.labels, label];
            utility.storage.setJson('__labels', state.labels);
        },
    },
})
export default taskLabelStore;

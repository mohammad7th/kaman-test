import { createStore } from 'vuex';
import utility from '@/util';

const taskLabelStore = createStore({
    state() {
        return {
            month: 0,
        }
    },
    getters: {
        getMonth(state): number {
            return state.month;
        }
    },
    mutations: {
        change(state: any, month: number) {
            state.month = month;
        },
    },
})
export default taskLabelStore;

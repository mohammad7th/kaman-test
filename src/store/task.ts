import { Task } from '@/models';
import { createStore } from 'vuex';
import utility from '@/util';

const taskStore = createStore({
    state() {
        return {
            searchValue: "",
        }
    },
    getters: {
        getSearchValue(state): string {
            return state.searchValue;
        },
        getTaskList(): Task[] {
            return utility.storage.getJson("__tasks") || [];
        }
    },
    mutations: {
        search(state: any, value: string) {
            state.searchValue = value;
        },
    },
})
export default taskStore;

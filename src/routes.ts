import { Path } from "./constant";
import Home from "./views/Home.vue";
const Route = [
    { title: "Home", path: Path.home, component: Home, meta: {}, },
];
export default Route;
